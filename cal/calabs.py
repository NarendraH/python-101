from abc import ABC, abstractmethod


class Cal(ABC):

    def __init__(self):
        print('init CAL')

    @abstractmethod
    def add(self):
        pass

    @abstractmethod
    def mult(self):
        pass

    @abstractmethod
    def dev(self):
        pass