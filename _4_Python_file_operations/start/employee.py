# import modules

from tkinter import *
import os
import sqlite3


# Designing window for registration

def register():
    global register_screen
    register_screen = Toplevel(main_screen)
    register_screen.title("Register")
    register_screen.geometry("500x350")

    global id
    global name
    global age
    global address
    global salary

    global id_entry
    global name_entry
    global age_entry
    global address_entry
    global salary_entry

    id = StringVar()
    name = StringVar()
    age = StringVar()
    address = StringVar()
    salary = StringVar()

    Label(register_screen, text="Please enter details below", bg="blue").pack()
    Label(register_screen, text="").pack()

    Label(register_screen, text="Id").pack()
    id_entry = Entry(register_screen, textvariable=id)
    id_entry.pack()

    Label(register_screen, text="Name").pack()
    name_entry = Entry(register_screen, textvariable=name)
    name_entry.pack()

    Label(register_screen, text="Age").pack()
    age_entry = Entry(register_screen, textvariable=age)
    age_entry.pack()

    Label(register_screen, text="Address").pack()
    address_entry = Entry(register_screen, textvariable=address)
    address_entry.pack()

    Label(register_screen, text="Sallary").pack()
    salary_entry = Entry(register_screen, textvariable=salary)
    salary_entry.pack()

    Button(register_screen, text="Register", width=10, height=1, bg="blue", command=register_user).pack()
    Button(register_screen, text="clear", width=10, height=1, bg="blue", command=clear).pack()

def clear():
    id_entry.delete(0, END)
    name_entry.delete(0, END)
    age_entry.delete(0, END)
    address_entry.delete(0, END)
    salary_entry.delete(0, END)


def register_user():
    store_details(id.get(), name.get(), age.get(), address.get(), salary.get())
    Label(register_screen, text="Registration Success", fg="green", font=("calibri", 11)).pack()



def store_details(id, name, age, address, salary):
    conn = sqlite3.connect('emp_db.db')
    query = ('INSERT INTO Employee (ID,NAME,AGE,ADDRESS,SALARY) '
             'VALUES (:ID, :NAME, :AGE, :ADDRESS, :SALARY);')
    params = {
        'ID': id,
        'NAME': name,
        'AGE': age,
        'ADDRESS': address,
        'SALARY': salary
    }

    conn.execute(query, params)

    conn.commit()
    print("Records created successfully")
    conn.close()


def main_account_screen():
    global main_screen
    main_screen = Tk()
    main_screen.geometry("300x200")
    main_screen.title("Account Login")
    Label(text="Select Your Choice", fg="white", bg="green", width="300", height="2", font=("Calibri", 13)).pack()
    Label(text="").pack()
    Button(text="Register", height="2", width="30", command=register).pack()

    main_screen.mainloop()


main_account_screen()