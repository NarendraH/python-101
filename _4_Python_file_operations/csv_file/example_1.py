"""
Reading from a CSV file is done using the reader object.
The CSV file is opened as a text file with Python’s built-in open() function,
which returns a file object. This is then passed to the reader, which does the heavy lifting.
"""
import csv

with open('employee_birthday.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(type(row))
            print(f'Column names are {"::".join(row)}')
            line_count += 1
        else:
            print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
            line_count += 1
    print(f'Processed {line_count} lines.')