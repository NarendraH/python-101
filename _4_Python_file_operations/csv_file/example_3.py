"""
writing csv file
"""
import csv

with open('employee_file5.csv', mode='w') as employee_file:
    emp = csv.writer(employee_file, delimiter=',')

    ## from where the data is comming
    emp.writerow(['id', 'name', 'sal'])
    emp.writerow(['123', 'john', '20k'])
    emp.writerow(['1234', 'mark', '30k'])


