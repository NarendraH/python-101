"""
Reading the CSV into a pandas DataFrame is quick and straightforward:
"""
import pandas

df = pandas.read_csv('hr_data.csv')
print(df.shape)
