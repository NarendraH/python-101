# import load_workbook
from openpyxl import load_workbook, Workbook
# set file path
wb=Workbook()

sheet=wb.active

filepath="demo1.xlsx"
# load demo.xlsx
wb=load_workbook(filepath)
# create new sheet
wb.create_sheet('Sheet 2')
# save workbook
wb.save("demo1.xlsx")