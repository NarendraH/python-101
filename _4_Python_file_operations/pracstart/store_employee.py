import csv
with open('employee1.csv', mode='a+') as csvfile:
    col_name = ['id', 'name', 'intime', 'outtime']
    emp_writer = csv.DictWriter(csvfile, fieldnames=col_name)
    emp_writer.writeheader()

    """
    here is the logic to get the data
    from cmd, tk, web
    """
    id = input('enter the id: ')
    name = input('enter the name: ')
    intime = input('enter the intime: ')
    outtime = input('enter the outtime: ')

    emp_dict ={
        'id':id,
        'name':name,
        'intime':intime,
        'outtime':outtime
    }

    emp_writer.writerow(emp_dict)