import sqlite3 as sq
from flask import Flask, request, g
from employee_server.utils.uitils import *
import json

def fetch_emp_list():
    try:
        cursor = g.db.execute('SELECT ID, NAME, DOB, ADDRESS, SALARY FROM Employee;')
    except:
        if request.content_type != JSON_MIME_TYPE:
            return onerror_response('No data available')


    employees = [{
        'ID': row[0],
        'NAME': row[1],
        'DOB': row[2],
        'ADDRESS': row[3],
        'SALARY': row[4]
    } for row in cursor.fetchall()]

    #return json.dumps(employees)
    return onsuccess_response(employees)



def store_employee():
    """
    store the details of employee
    :return:
    """
    data = request.json
    print(data)
    print(request.json)
    query = ('INSERT INTO Employee (ID,NAME,DOB,ADDRESS,SALARY) '
             '' 'VALUES (:ID, :NAME, :DOB, :ADDRESS, :SALARY);')

    params = {
        'ID': data['ID'],
        'NAME': data['NAME'],
        'DOB': data['DOB'],
        'ADDRESS': data['ADDRESS'],
        'SALARY': data['SALARY']
    }

    g.db.execute(query, params)

    return fetch_emp_list()

def get_data_origin():
    """
    This functon is ment for the local cost item and the replacement of the
    local card employee details.
    :return:
    """