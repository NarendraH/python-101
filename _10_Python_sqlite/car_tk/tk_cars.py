from tkinter import *
from car_tk.insert_data import store_data

class MainGUI:


    def __init__(self,master):
        self.var1=StringVar()
        self.var2=StringVar()
        self.var3=IntVar()


        self.label1=Label(master,text='Car Name')
        self.label1.grid(row=0,column=0)

        self.entry1=Entry(master,text='',textvariable=self.var1)
        self.entry1.grid(row=0,column=1)

        self.label2 = Label(master, text='Car Type')
        self.label2.grid(row=2, column=0)

        self.entry2 = Entry(master, text='', textvariable=self.var2)
        self.entry2.grid(row=2, column=1)

        self.label3 = Label(master, text='Car Cost')
        self.label3.grid(row=3, column=0)

        self.entry3 = Entry(master, text='', textvariable=self.var3)
        self.entry3.grid(row=3, column=1)

        self.btn1=Button(master,text='Submit',command=self.submit)
        self.btn1.grid(row=6,column=0)

        self.btn2=Button(master,text='Show',command=self.show)
        self.btn2.grid(row=6,column=1)

        self.btn3=Button(master,text='Clear',command=self.clear)
        self.btn3.grid(row=6,column=2)

        self.lbl=StringVar()
        self.label4=Label(master,textvariable=self.lbl)
        self.label4.grid(row=10,column=1)

    def submit(self):
        self.car_name=self.var1.get()
        self.car_type=self.var2.get()
        self.car_cost=self.var3.get()
        param={
            'NAME':self.car_name,
            'TYPE':self.car_type,
            'COST':self.car_cost
            }
        print(self.car_name,self.car_type,self.car_cost)
        store_data(param)



    def show(self):
        self.text=self.car_name,self.car_type,self.car_cost
        self.lbl.set(self.text)
        return self.lbl


    def clear(self):
        self.entry1.delete(0,END)
        self.entry2.delete(0,END)
        self.entry3.delete(0,END)


root=Tk()
root.geometry('500x500')
app=MainGUI(root)
root.mainloop()
