"""
Double Pre Underscore
__name

Double Pre Underscores are used for the name mangling.

Double Pre Underscores tells the Python interpreter to rewrite the
attribute name of subclasses to avoid naming conflicts.

Name Mangling:- interpreter of the Python
alters the variable name in a way that it is challenging to
clash when the class is inherited.

"""


class Sample():
    def __init__(self):
        self.a = 1
        self._b = 2
        self.__c = 3
        print('hello sample')


obj1 = Sample()
print(obj1.a)
print(obj1._b)
#print(obj1.__c)

class SecondClass(Sample):
    _a = 10
    def __init__(self):
        Sample.__init__(self)
        self.a = "overridden"
        self._b = "overridden"




obj2 = SecondClass()
print(obj2.a)
print(obj2._b)
#print(obj2.__c)
print(obj2._a)
