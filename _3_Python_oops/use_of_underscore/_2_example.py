# Single Pre Underscore is used for internal use.
# Most of us don't use it because of that reason.
class Test:
    a = 10
    def __init__(self):
        self.name = "datacamp"
        self._num = 7
        print('yes calling')

    def __some(self):
        print('some')

    def somethingmore(self):
        self.__some()
        print('somethingmore')

obj = Test()
print(obj.name)
print(obj._num)
print(obj.a)
print(obj.somethingmore())





