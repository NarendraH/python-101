class A(object):

    def __init__(self):
        print('A')

    def funcA(self):
        print('finc a')


class B(A):

    def __init__(self):
        print('B')

    def funcB(self):
        print('finc B')

class C(A, B):

    def __init__(self):
        print('c')

    def func(self):
        A.funcA()


c = C()
c.func()

#
# class A(object):
#     def say_hello(cls):
#         print ('A says hello')
#
#
# class B():
#      def say_hello(cls):
#         print ('B says hello')
#
#
# class C(B):
#     def say_hello(cls):
#         super(C, cls).say_hello()
#         print ('C says hello')
#
#
# class D(A, C):
#     def say_hello(self):
#         super(D, self).say_hello()
#         print ('D says hello')
#
#
# DD = D()
# DD.say_hello()