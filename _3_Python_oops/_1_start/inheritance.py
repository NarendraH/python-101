class Vehicle:

    def __init__(self, name):
        self.name = name

    def __name_of_car__(self):
        print('name of the car is from Vehicle{}'.format(self.name))

    def spee_of_car(self, speed):
        print('speed of the car is {}'.format(speed))

class Car(Vehicle):

    def __init__(self, name, speed):
        self.name = name
        self.speed = speed

    def __has_auto_speech__(self, flag):
        print('has auto speech {}'.format(flag))



#Car cara = Car('BMW', 200)

v = Vehicle('Unknown')
v.__name_of_car__()
v.spee_of_car(20)

c = Car('BMW', 300)
c.__name_of_car__()
c.spee_of_car(20)
c.__has_auto_speech__('yes')






