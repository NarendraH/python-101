class Person:

    # attribute of a class
    iam = "person"

    # instance of a class
    def __init__(self, name, age, char):
        self.name = name
        self.age = age
        self.char = char

    def isAProgrammer(self):
        return 'yes {}'.format(self.name)+' is a programer {}'.format(self.age)+' and {}'.format(self.char)

personMain = Person('Narendra', 20, 'good')
print('data ', personMain.isAProgrammer())
