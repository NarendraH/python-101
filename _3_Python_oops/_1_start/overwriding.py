class Parent(object):
    def __init__(self):
        print('init parant')
        self.value = 4

    def get_value(self):
        print('get value p ')
        return self.value


class Child(Parent):

    def get_value(self):
        print('get value c')
        return self.value + 1

a = Parent()
print(a.get_value())

b = Child()
print(b.get_value())

a = Child()
print(a.get_value())
