class Test(object):

    a = 10
    _b = 20
    __c = 30

    def test_class(self):
        print('class')
        self.d = 40
        self._e = 50
        self.__f = 60


t = Test()
print(t.a)
print(t.d)