from abc import ABC, abstractmethod

class AbstractClassExample(ABC):

    def __init__(self, value):
        self.value = value
        super().__init__()
        print('i am init AbstractClassExample')

    @abstractmethod
    def do_something(self):
        pass


class test(AbstractClassExample):

    def do_something(self):
        print('i am overwridden')


t = test(value=10)
t.do_something()

